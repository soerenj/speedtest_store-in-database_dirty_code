# speedtest_store-in-database_dirty_code

![ ](screenshots/symbolic.png)

requirements:
- This is for a php/mysql for server-Backend (you need a own Web-server)
- This is for test-devices, which can execute a shellscript. e.g. router, like openwrt

Use:
- you can use for **monitor an internet-Access**/Connection (also multiple). 
- make a very simple speedtest: count time downloading a testfile (duration); & save the result in database.
- dirty code;

Screenshot:
 ![Screenshot](screenshots/screenshot5.png)


 Function:
 - Alarm on offline : it will send you an Email at Device-offline (not for multiple devices)
 - simple Graph (speed/ping+RXTraffic (not in this Screenshot) )
 ![Screenshot simple graph](screenshots/screenshot4.png)
 - data Table
 ![Screenshot table data, single line](screenshots/screenshot_line.png)
 - simple Graph for multiple devices (speed)
 ![Screenshot simple graph, for multiple devices](screenshots/graph_two-devices.png)
 - (optional:) export(csv)
 - (optional:) delete old data (manuell by hand)

 Server-Site needed:
 - PHP
 - mysql (or simular)
 - (optional:?) Cron-job
 
 
 Speedtest Quick and Dirty (not very accurate)
 - download the Testfile
 - measure time for Download;
 - other values: ping
 - other values (only on openwrt?): current connected wifi-station, wifi-signal-strength , traffic-Count

 

Installation in short
 - you musst change every "xxx", with you own configuration (in all .php files)
 - Test-Device (e.g. router, like OpenWrt):
   - insert the shellscript in your Test-Device ; _( config: server-adresse/url, Test-File-Size )_ 
   - _(( alternativ you find the shellscript: beginning of the main  _speedtest_table_list_save.php_ file ))_
   - add to crobjob list
 - Server:
   - PHP: config main _speedtest_table_list_save.php_ file  _( config: database )_
   - SQL: create database & tables _(e.g. via phpMyAdmin)_
    script: sql_create_tables__install_query.sql
   - SQL: _(( if the query.sql seems old-version? look at end of the main _speedtest_table_list_save.php_ file))_
   - (optional?:) add _cronjob_email-alert.php_ to Cronjob list  _( config: server-adresse/url, Emailadress )_
   - (optional: create more test-files via `dd` e.g.:  `dd if=/dev/zero of=0.1.mb  bs=100k  count=1` ; note: 0.1.mb give unaccurate speedtest)
 

details/functions of Graph:
 - with graph for Ping (see below)
 - marked changed of conntected wifi-station (first text line in graph; only the end of the mac-adress)
 - multiple devices (simultan) support: you got multiple graphs and multiple tables
 - offline time: colored (red/orange)
 - set max range in Graph
 - go back in past (link "past")


Quick and Dirty written



Screenshots:
![Screenshot](screenshots/screenshot.png)

![Screenshot3 only Graph, with ping Graph](screenshots/screenshot3.png)

![Screenshot2 simple graph](screenshots/screenshot2.png)



-----------------------------------------
----- in german (unvollständig) ------
-----------------------------------------


# speedtest_store-in-database_dirty_code
Dirty Code; Amache einen einfachen Geschwindikeit-Test der Internet-Verbindung (Zähle die Download Zeit einer Testdatei) & speichere diese Ergebnisse in einer Tabelle (z.B. von routern, wie openwrt); php/mysql für server-Backend

Zum überwachen eines/mehrerer Internet-Anschlusses/Verbindung

 Funktionen:
 - Alarm bei offline : sende eine Email, wenn das Gerät offline ist
 - Einfacher Graph / Zeichung
 ![Screenshot4 Graph](screenshots/screenshot4.png)
 - data Tabelle
 ![Screenshot Table Data single Line](screenshots/screenshot_line.png)
  

in Graph/Data-Table:
 - Mit Zeichnung für Ping-zeiten (sehe Bildschirmfotos)
 - markiere neu verbundenen Acess-Point in Zeichnung (erste zeile in Zeichnung; letzten Zahlen der mac-adresse)
 - setzte maximale Höhe in Graph 
 - offline time: farblich markiert (rot/orange)
 - Zeige alte Daten (link "past")
 - mehrere Geräte gleichzeitig werden unterstützt: man bekomme mehrer ZEichnungen und Tabellen

If you want a [graph Wifi-Station-Strength](https://gitlab.com/soerenj/speedtest_store-in-database_dirty_code/snippets/1836306) of all Wifi-stations around you, with table of rawdata.

Programmcode schnell und unsauber geschrieben
