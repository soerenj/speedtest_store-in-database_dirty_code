#!/bin/sh
#https://gitlab.com/soerenj/speedtest_store-in-database_dirty_code/


  #create the test-Download files:
  ## dd if=/dev/zero of=0.1.mb  bs=100k  count=1  #(not recommend: 0.1.mb File to small for a good test result)
  ## dd if=/dev/zero of=1.mb  bs=1M  count=1
  ## dd if=/dev/zero of=5.mb  bs=1M  count=5
  ## dd if=/dev/zero of=10.mb  bs=1M  count=10


speedtest () {                                                                     
  #echo "a"                                                                
  #  #there a two version:                                                                                               
  #   # a) measure only seconds                                                 
  #   # b) measure seconds, eith one dot (e.g. 0.1 seconds)                
  #   #    for (b) you need to install "coreutils-date"                    
    #possible: 0.1 0.5 1 5 10 (what ever testfiles you created before)                                    
    t_start=$(date +%s%3N)                                                 
    t_start_s=$(date +%s)                                                                              
    t_start_n=$(date +%3N)                                                 
    size="0.1" #fuer Log (s.u.)                                            
    wget http://xxxx.de/${size}.mb -O /dev/null   
	#on multi interfaces: curl --interface $DEVICE  http://xxxx.de/${size}.mb -o /dev/null
	t_end=$(date +%s%3N)
	t_end_s=$(date +%s)
    t_end_n=$(date +%3N)
    if [ -e "/usr/bin/date" ]
	  then
        t_total_s=$(expr $t_end_s - $t_start_s)
        if [ $t_end_n -lt $t_start_n ]
        then
              t_total_n=$(expr $t_start_n - $t_end_n)
        else
              t_total_n=$(expr $t_end_n - $t_start_n)
        fi
        t_total=$t_total_s.$t_total_n
    else
        t_total=$(expr $t_end - $t_start)
    fi
    #echo $t_total
    #output only rounded Count (e.g. 1, for ~1seconds)
    #if()
	  addr=$(iw dev $DEVICE link | grep "Connected to " )
    addr=${addr:13:17}                                           
    signal=$(iw dev $DEVICE link | grep "signal: " )                                                
    signal=${signal:10:2}
    traffic=$(ifconfig $DEVICE | egrep -r -o "RX bytes:[0-9]* \(.*\)" | egrep -r -o "(\([^\)]*)\)" | tr -d '\n' | tr  ' ' '+' )
    packets=$(ifconfig $DEVICE | egrep -r -o "RX packets:[0-9]*.*" | egrep -r -o "[0-9]*"  | sed ':a;N;$!ba;s/\n/+/g')
    ping=$(ping 8.8.8.8 -I $DEVICE -c 1 | egrep -r -o "time.(.*)" | egrep -r -o "([0-9]*)\." | egrep -r -o "[0-9]*")
    rx_raw=$(ifconfig $DEVICE | egrep -r -o "RX bytes:[0-9]*" | egrep -r -o "[0-9]*")
    tx_raw=$(ifconfig $DEVICE | egrep -r -o "TX bytes:[0-9]*" | egrep -r -o "[0-9]*")
    traffic_raw="${rx_raw}+${tx_raw}"                
  
    wget "http://xxx.de/speedtest_table_list_save.php?size=$size&duration=$t_total&interface=$INTERFACE&host=$HOSTNAME&addr=$addr&signal=$signal&traffic=$traffic&packets=$packets&ping=$ping&traffic_raw=$traffic_raw" -O /dev/null
    
  }       
