<?php
/*
 * Speedtest, store Data in Database (dirty code)
 *
 * https://gitlab.com/soerenj/speedtest_store-in-database_dirty_code
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation.  Please see LICENSE.txt at the top level of
 * the source code distribution for details.
 */

  #basiert on: https://draeger-it.blog/wemos-d1-wlan-thermometer-teil2-upload-der-daten-in-eine-datenbank/
  #my parts: Lizenz GPL2/3, AGPL, Creative Commons, PublicDomain, CC-0
  #php mysql #save data
  #create the test-Download files:
  ## dd if=/dev/zero of=0.1.mb  bs=100k  count=1  #(not recommend: 0.1.mb File to small for a good test result)
  ## dd if=/dev/zero of=1.mb  bs=1M  count=1
  ## dd if=/dev/zero of=5.mb  bs=1M  count=5
  ## dd if=/dev/zero of=10.mb  bs=1M  count=10
  /* shell:
  #####!/bin/sh
  #https://gitlab.com/soerenj/speedtest_store-in-database_dirty_code/
  speedtest () {                                                                     
  #echo "a"                                                                
  #  #there a two version:                                                                                               
  #   # a) measure only seconds                                                 
  #   # b) measure seconds, eith one dot (e.g. 0.1 seconds)                
  #   #    for (b) you need to install "coreutils-date"                    
    #possible: 0.1 0.5 1 5 10 (what ever testfiles you created before)                                    
    t_start=$(date +%s%3N)                                                 
    t_start_s=$(date +%s)                                                                              
    t_start_n=$(date +%3N)                                                 
    size="0.1" #fuer Log (s.u.)                                            
    wget http://xxxx.de/${size}.mb -O /dev/null   
	  #on multi interfaces: curl --interface $DEVICE  http://xxxx.de/${size}.mb -o /dev/null
	  t_end=$(date +%s%3N)
	  t_end_s=$(date +%s)
    t_end_n=$(date +%3N)
    if [ -e "/usr/bin/date" ]
	  then
        t_total_s=$(expr $t_end_s - $t_start_s)
        if [ $t_end_n -lt $t_start_n ]
        then
              t_total_n=$(expr $t_start_n - $t_end_n)
        else
              t_total_n=$(expr $t_end_n - $t_start_n)
        fi
        t_total=$t_total_s.$t_total_n
    else
        t_total=$(expr $t_end - $t_start)
    fi
    #echo $t_total
    #output only rounded Count (e.g. 1, for ~1seconds)
    #if()
	  addr=$(iw dev $DEVICE link | grep "Connected to " )
    addr=${addr:13:17}                                           
    signal=$(iw dev $DEVICE link | grep "signal: " )                                                
    signal=${signal:10:2}
    traffic=$(ifconfig $DEVICE | egrep -r -o "RX bytes:[0-9]* \(.*\)" | egrep -r -o "(\([^\)]*)\)" | tr -d '\n' | tr  ' ' '+' )
    packets=$(ifconfig $DEVICE | egrep -r -o "RX packets:[0-9]*.*" | egrep -r -o "[0-9]*"  | sed ':a;N;$!ba;s/\n/+/g')
    ping=$(ping 8.8.8.8 -I $DEVICE -c 1 | egrep -r -o "time.(.*)" | egrep -r -o "([0-9]*)\." | egrep -r -o "[0-9]*")
    rx_raw=$(ifconfig $DEVICE | egrep -r -o "RX bytes:[0-9]*" | egrep -r -o "[0-9]*")
    tx_raw=$(ifconfig $DEVICE | egrep -r -o "TX bytes:[0-9]*" | egrep -r -o "[0-9]*")
    traffic_raw="${rx_raw}+${tx_raw}"                
  
    wget "http://xxx.de/speedtest_table_list_save.php?size=$size&duration=$t_total&interface=$INTERFACE&host=$HOSTNAME&addr=$addr&signal=$signal&traffic=$traffic&packets=$packets&ping=$ping&traffic_raw=$traffic_raw" -O /dev/null
    
  }                 
  */
  /*
  Cronjob.php für email-Alert:
  <?php
  #https://gitlab.com/soerenj/speedtest_store-in-database_dirty_code/
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  require_once('speedtest_table_list_save.php');

  $maxoffline = 6; //hour
  echo "<h1>Cronjob</h1><span style='background:#cccccc'>Email-alarm if offline XXX</br>offline at least $maxoffline hours <br>this cronjob must be called frequently<br>Email will be send only ONCE. For futher emails you must reset the alarm.</span></br><br>";
  $file_state = 'cronjob_email_state.txt';
  if(isset($_GET['reset']) && $_GET['reset']=="1"){
	  if(unlink($file_state))echo "<span style=\"color:green\"> $file_state alarm was resetet</span>.";
	  else echo'error delete file '.$file_state;
  }

	
      if(!is_writable('./')) die("Folder not Writable");
      

      if(file_exists($file_state)) die( "Last Email-Alert, was already send (an musst be reseted) <a href='cronjob_email-alert.php?reset=1'>Reset Email alert: cronjob_email-alert.php?reset=1</a>" );

      getDBConnection();
      $stmt = "SELECT * FROM `data_speedtest` ORDER BY timestamp DESC LIMIT 1";
      //$stmt .= "(`id`, `size`, `duration`, `interface`,`timestamp`)";
      $connection = getDBConnection();
      $result = $connection->query($stmt);  

	  while($row = ($result->fetch_assoc())){
                            echo '<br>Last Message <em>Online</em> from XX '.date('d.m.Y  H:i',$row['timestamp']).'<br>';
		  if( $row['timestamp']+60*60*$maxoffline < time() ){
			  //ist wohl offline
			  //date('D.m.y H:i',$row['timestamp'])
			  $text = 'here is writeing a automtaic script.
             The Internet is since ~'.$maxoffline.'h offline
			
			  the email will be send only ONCE.
              You must reset this alart, for further emails:
              http://xxxx/cronjob_email-alert.php?reset=1
			
			
              http://xxxx.de/speedtest_table_list_save.php
			  ';
			
			  if( mail('your-emailadresse@example.com, your-emailadresse2@example.com','XXX: Internet offline?',$text) ) echo "<p>alaert-email was send</p>"; else echo "failed to send alart-Email";
			  $myfile = fopen($file_state, "w") or die("Unable to open file! $file_state");
			  $txt = "Error mail was send\n";
			  fwrite($myfile, $txt);
			  fclose($myfile);


		  }
	  }	
  echo "<br>cronjob ende";
  */


  //Database Config
  $dbname = "xxx"; 
  $dbusername = "xxx";
  $dbpassword = 'xxx';
  $dburl=''; //leave empty, if localhost/same host

  //cronjob currently disbaled (http://.../cronjob.php) eintragen

  $welcome_text = "<h1>welcome</h1>";

  $allow_delete=false;//show option to delete old Data
  $allow_export=false;//show option to export data as csv

  error_reporting(E_ALL);
  error_reporting(-1);
  ini_set('display_errors', 1);
  error_reporting(-1);
  date_default_timezone_set('Europe/Berlin');
  
  // ----------------
  // ----------------
  // all options are optional... for the beginnen, let them just disabled
  // ----------------
  // ----------------
  
  /* disabled options
  // sleep-time between measure-Point
  //    (only needed for highlight offline-times)
  $measure_eachMin = 2; //minutes
  $measure_eachMin_device['OpenWrt1'] = 2; //(optional)Measure each minutes; special for one host
  $measure_eachMin_device['OpenWrt2'] = 10;
  
  // addational second sleep-time between measure-Point (optional) 
  //     (.e.g you want you don't/rare test in the night)
  $measure_eachMin_second = 10; //minutes
  $measure_eachMin_second_timerange = array(0,1,2,3,4,5,6,7); //hours, in that this Measure-Sleep-Time is valid
  
  // graph: max showed value  (if lower, than automatic scale up) (optional) 
  $draw_maxValue = array('0.1'=>6);  //2.13333 //(optional) statistic draw (max size to show)
                                      //e.b. array('0.1'=>4) for 0.1MB within max 4 seconds
  $draw_maxValue__sync = true; //all graphs have the same MaxValues
  
  
  $file_state = 'cronjob_email_state.txt'; //just needed for email-alarm;
  
  
  // graph: set help-lines (optional) 
  $backgroundLinesPersonal = array();
  $backgroundLinesPersonal['OpenWrt1'] = array();
  $backgroundLinesPersonal['OpenWrt1']['normal'] = array(2000,3000);
  $backgroundLinesPersonal['OpenWrt1']['ping']   = array(30);
  $backgroundLinesPersonal['OpenWrt2'] = array();
  $backgroundLinesPersonal['OpenWrt2']['normal'] = array(2000,3000);
  $backgroundLinesPersonal['OpenWrt2']['ping']   = array(30);
  $backgroundLinesPersonal['raw']['OpenWrt1'] = array(); //show_raw_values
  $backgroundLinesPersonal['raw']['OpenWrt1']['normal'] = array(0.3,1.6);
  $backgroundLinesPersonal['raw']['OpenWrt2'] = array();
  $backgroundLinesPersonal['raw']['OpenWrt2']['normal'] = array(0.3,1.6);
  
  $config_hide_in_draw__empty_items = true; //hide empty (or 0)-elements in graph-draw
  */

  if( isset($_GET['statsimage'])){readDataAndDrawStat();exit;}


if(isset($_GET["export"]) && $allow_export===true){
	if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {ob_start('ob_gzhandler');} //does this work?
 	$stmt = "SELECT * FROM `data_speedtest` ORDER BY timestamp";
    $connection = getDBConnection();
    $result = $connection->query($stmt); if($result===false)return array();
	header("Content-type: text/csv");
	header('Content-Disposition: attachment; filename="speedtest_export_'.date('y-m-d_H-i').'.csv"');
	$isFirstRow=true;
	while($row = ($result->fetch_assoc())){
		if($isFirstRow){
		  foreach($row as $key=>$item){
		     echo $key;
			 if(array_key_last($row)!=$key) echo",";
		  }
		  echo ",datetime";
		  echo "\n";
		}
		foreach($row as $key=>$item){
		   echo $item;
		   if(array_key_last($row)!=$key) echo",";
		}
		echo ",".date("d.m.Y H:i:s",$row["timestamp"])."";
		echo "\n";
		$isFirstRow=false;
	}
    die("");
}



if(isset($_POST['delete_older_then']) && $allow_delete===true){
  $_POST['delete_older_then']=(int)$_POST['delete_older_then'];
  if($_POST['delete_older_then']==0) die("nothing to delete, please go back");
  if($_POST['delete_older_then']<7) die("nothing to delete, lower then last 7Days not allowed to delete. please go back");
  echo "<hr>";
  echo "<h1 style=\"color:red\">DELETE Data older then ".(int)$_POST['delete_older_then']." days<h1>";
	getDBConnection();
    $stmt = "DELETE FROM `data_speedtest` WHERE `timestamp` < ".(time()-($_POST['delete_older_then']*60*60*24))." ORDER BY timestamp DESC";
    $connection = getDBConnection();
    $result = $connection->query($stmt);
    //echo $stmt;
	if($result===true)echo "<p>erfolgreich durchgeführt</p>"; else "<p>Fehler?</p>";
	var_dump($result);
  echo "<hr>";
}



  echo '<!DOCTYPE html><html>
  <head><meta name="robots" content="noindex"><style>table tr:nth-of-type(odd)  {background-color: #F5E7CC;} </style></head><body>';


  if($file_state!='' && file_exists($file_state)) echo( "<p>Last Email-Alert, was already send (and musst be reseted) <a href='cronjob_email-alert.php?reset=1'>Reset Email alert: cronjob_email-alert.php?reset=1</a></p>" );


  if (basename($_SERVER["SCRIPT_FILENAME"], '.php') == "cronjob") $isCron = true;
  else $isCron = false;

  if(!$isCron){
	  if (empty($_GET) || ((!isset($_GET['size']) && !isset($_GET['duration'])))) {
		  $offset=''; if(isset($_GET['offset'])) $offset=(int)$_GET['offset'];
		  echo $welcome_text;
		  echo readData($offset);
		  
		  $sampleLink = "...upload.php?size=0.10&duration=0.150&interface=wlan0-1";
		  echo "<hr><details><summary>How to save/store Data (click to expand)</summary><font size=2 color=\"#555555\"><br> 
		  possible parameter: size - Test-file size (in MB,  with two numbers after \",\" ) ;
		  duration - needed duration for downlod (round, in secunds, with numbers behind the \",\");
		  interface (Wlan Interface (unimportant), and more: signal / addr / ping / traffic /
		  packets / action(=7char) (see sourceCode);&nbsp;&nbsp;&nbsp;&nbsp; zbsp.: $sampleLink</font></details>";
		  if($allow_delete===true)echo "<details><summary>Delete Data (click for details): </summary>
<span class=\"opacity_nonhover\">Clean Up Database: delete all data older then 
<form method=\"post\" id=\"form_delete\" style=\"display:inline\"><select name=\"delete_older_then\" id=\"delete_older_then\">
<option value=\"\">-</option><option value=\"1085\">3year</option><option value=\"720\">2year</option><option value=\"365\">1year</option><option value=\"182\">1/2year</option><option value=\"90\">90days</option><option value=\"30\">30days</option><option value=\"7\">7days</option><option value=\"\">-</option>
</select>
<input type=\"button\" value=\"delete\" onClick=\"if(document.getElementById('delete_older_then').value>0 && prompt('DELETE the Data? \\n(can NOT be undone)\\nplease write \\'delete really\\'')=='delete really')document.getElementById('form_delete').submit()\"></form></span>
</details>
<hr>";
if($allow_export===true)echo "<font size=2 color=\"#555555\"><a href=\"?export=csv\">Export Data(csv)</a></font></br>";

	     echo "<br><font size='1'><a href='https://gitlab.com/soerenj/speedtest_store-in-database_dirty_code' rel='noreferrer'>source code (AGPL) </a> <i> </i></font>";
	  } else {
		  //#justDownload seems to be defect? infinit loop in store? No Data will be send...
		  //if(isset($_GET['justDownload'])){
		  //    header('Content-Description: File Transfer');
          //    header('Content-Type: application/octet-stream');
		  //	$_GET['size'] = str_replace('mb','',$_GET['size']);
		  //}else 
		  echo "<h2>Parmeter recieved:</h2>";
		  $sizeValue = 0;
		  $durationValue = 0;
		  $interfaceValue = 0;
		  date_default_timezone_set('Europe/Berlin');
		  $milliseconds = round(microtime(true));
		  $hostValue=''; $addrValue=''; $signalValue='';$trafficRXValue='';$trafficRXMultiplikator='';$trafficTXValue='';$trafficTXMultiplikator='';$packetsRX_total='';$pingValue='';$actionValue='';$trafficRX_rawValue='';$trafficTX_rawValue='';
		  foreach ($_GET as $key => $value) { 
			  echo "Parameter: ";
			  echo strip_tags(substr($key,0,9));
			  echo "<br/>Value:";
			  if ($key=='interface' || $key=='traffic_raw') echo strip_tags(substr($value,0,15));
			  else if ($key=='traffic') echo strip_tags(substr($value,0,30));
		      else    echo strip_tags(substr($value,0,9));
			  echo "<br/><br/>";
			  if($key === 'size'){
			    $sizeValue = $value;
		    } else if($key === 'duration'){
		      $durationValue = $value;
		    } else if($key === 'interface'){
		      $interfaceValue = $value;
	        }else if($key === 'host'){
		      $hostValue = $value;
	        }else if($key === 'addr'){
		      $addrValue = trim(str_replace('addr','',$value));
	        }else if($key === 'signal'){
		      $signalValue = trim(str_replace('','',$value));
	        }else if($key === 'ping'){
		      $pingValue = trim(str_replace('','',$value));
		    }else if($key === 'action'){
		      $actionValue = trim(str_replace('','',$value));
		    }else if($key === 'traffic'){
	        //RX bytes:3229426 (3.0 MiB)  TX bytes:17250036 (16.4 MiB)
	        //(11.6 MiB)(1.2 MiB)
		      $traffic = $value;
			  $traffic = explode(')(',$traffic);
		      $trafficRX = explode(' ',str_replace('(','',$traffic[0]));
		      $trafficTX = explode(' ',str_replace('(','',$traffic[1]));
		      $trafficRXValue = $trafficRX[0];//explode('.',$trafficRX[0])[0];
		      $trafficTXValue = $trafficTX[0];// explode('.',$trafficTX[0])[0];
		      $trafficRXMultiplikator = substr($trafficRX[1],0,1);
		      $trafficTXMultiplikator = substr($trafficTX[1],0,1);
			}else if($key === 'traffic_raw'){
		      $traffic = $value;
			  $traffic = explode(' ',trim($traffic));
		      $trafficRX_rawValue = $traffic[0];
		      $trafficTX_rawValue = $traffic[1];
	        }else if($key === 'packets'){
	        //RX packets:36326 errors:0 dropped:0 overruns:0 frame:0 
	        //13576 0 0 0 0
		      $packets = $value;
		      $packets = explode(' ',$packets);
		      $packetsRX_total = trim($packets[0]);
		      //$packetsRX_error = trim($packets[1]);
		      //$packetsRX_drop  = trim($packets[2]);
	        }
			//else if($key === 'justDownload'){
		    //}
		  }
		  /*
		  //Packets: save only diff
		  if($packetsRX_total>0){
			  $lastRow = readData_getLastEntry($interfaceValue,$hostValue);
			  if($lastRow!=null && $lastRow['packetsRX_total']!=0){
				  if($packetsRX_total>=$lastRow['packetsRX_total']) $packetsRX_total = $packetsRX_total - $lastRow['packetsRX_total'];
				  if($packetsRX_error>=$lastRow['packetsRX_error']) $packetsRX_error = $packetsRX_error - $lastRow['packetsRX_error'];
				  if($packetsRX_drop>=$lastRow['packetsRX_drop'])   $packetsRX_drop = $packetsRX_drop - $lastRow['packetsRX_drop'];
			  }
		  }
		  */
		  //if(!isset($_GET['justDownload'])){
		  //  echo "Timestamp: ";
		  //  echo date('d.m.Y H:i:s',$milliseconds);
		  //  echo " (<i>automatic</i>)";
		  //  echo "<br/>";
		  //}
		  storeData($sizeValue, $durationValue, $interfaceValue, $milliseconds, $hostValue, $addrValue, $signalValue,$trafficRXValue,$trafficRXMultiplikator,$trafficRX_rawValue,$trafficTXValue,$trafficTXMultiplikator,$trafficTX_rawValue,$packetsRX_total,$pingValue,$actionValue);
		  //if(isset($_GET['justDownload'])){
		  //	  $e=explode('.',$_GET['size']);
		  //	  $f=substr( (int)$e[0] ,0, 4); //4=>max 9999MB
		  //	  if(isset($e[1]))$f.='.'.substr( (int)$e[1] ,0 ,1);
		  //	  echo file_get_contents( $f.'mb' );
		  //	  exit;
		  //}
	  }
  }


  function storeData($sizeValue, $durationValue, $interfaceValue, $milliseconds, $hostValue, $addrValue, $signalValue=0,$trafficRXValue=0,$trafficRXMultiplikator='',$trafficRX_rawValue=0,$trafficTXValue=0,$trafficTXMultiplikator='',$trafficTX_rawValue=0,$packetsRX_total=0,$pingValue=0,$actionValue=''){
      if(preg_match("#[^0-9,\.]#",$sizeValue) || preg_match("#[^0-9,\.]#",$durationValue)  || preg_match("#[^0-9a-zA-Z,_\-\.]#",$interfaceValue) || count($interfaceValue)>150 ){
        die("<font color=red><b>Fehler:</b></font>unallowed values (only 0-9 , . erlaubt); At Interface aditional a-Z_ and max. 15Counts");
      }
	  /* save Traffic raw as Diff? (was only a short unfinshed test)
	  $trafficTX_rawValue_diff='';
	  $trafficRX_rawValue_diff='';
	  if( $trafficTX_rawValue!=0 || $trafficRX_rawValue!=0 ){
	  	 $lastRow = readData_getLastEntry($interfaceValue,$hostValue,$sizeValue);
		 if($lastRow!='' && isset($lastRow->trafficTX_raw) ){
		 	$trafficTX_rawValue_diff = $trafficTX_rawValue-$lastRow->trafficTX_raw;
			$trafficRX_rawValue_diff = $trafficRX_rawValue-$lastRow->trafficRX_raw;
		 }
	  }*/
      $connection = getDBConnection();
	
      $stmt = "INSERT INTO `data_speedtest` ";
      $stmt .= "(`id`, `size`, `duration`,   `interface`, `timestamp`, `host`, `addr`, `signal`, `trafficRX`, `trafficRX_multiplicator`, `trafficRX_raw`, `trafficTX`, `trafficTX_multiplicator`, `trafficTX_raw`, `packetsRX_total`,`ping`,`action`)";
      $stmt .= " VALUES (";
      $stmt .= "NULL";
      $stmt .= ",'";
      $stmt .= mysqli_real_escape_string($connection,substr($sizeValue,0,9));
      $stmt .= "',";
      $stmt .= "'";
      $stmt .= mysqli_real_escape_string($connection,substr($durationValue,0,9));
      $stmt .= "',";
      $stmt .= "'";
      $stmt .= mysqli_real_escape_string($connection,substr($interfaceValue,0,15));
      $stmt .= "',";
      $stmt .= "'";
      $stmt .= $milliseconds;
      $stmt .= "',";
      $stmt .= "'";
      $stmt .= mysqli_real_escape_string($connection,substr($hostValue,0,35));
      $stmt .= "',";
      $stmt .= "'";
      $stmt .= mysqli_real_escape_string($connection,substr($addrValue,0,20));
      $stmt .= "',";
      $stmt .= "'";
      $stmt .= mysqli_real_escape_string($connection,substr($signalValue,0,3));
	  $stmt .= "',";
      $stmt .= "'";
      $stmt .= mysqli_real_escape_string($connection,substr($trafficRXValue,0,7));
	  $stmt .= "',";
      $stmt .= "'";
	  $stmt .= mysqli_real_escape_string($connection,substr($trafficRXMultiplikator,0,1));
	  $stmt .= "',";
      $stmt .= "'";
	  $stmt .= mysqli_real_escape_string($connection,substr($trafficRX_rawValue,0,15));
	  $stmt .= "',";
      $stmt .= "'";
      $stmt .= mysqli_real_escape_string($connection,substr($trafficTXValue,0,7));
	  $stmt .= "',";
      $stmt .= "'";
	  $stmt .= mysqli_real_escape_string($connection,substr($trafficTXMultiplikator,0,1));
	  $stmt .= "',";
      $stmt .= "'";
	  $stmt .= mysqli_real_escape_string($connection,substr($trafficTX_rawValue,0,15));
	  $stmt .= "',";
      $stmt .= "'";
      $stmt .= mysqli_real_escape_string($connection,substr($packetsRX_total,0,11));
	  $stmt .= "',";
      $stmt .= "'";
      $stmt .= mysqli_real_escape_string($connection,substr($pingValue,0,5));
	  $stmt .= "',";
      $stmt .= "'";
      $stmt .= mysqli_real_escape_string($connection,substr($actionValue,0,7));
      $stmt .= "'";
      $stmt .= ");";
      $result = mysqli_query($connection,$stmt);
      if($connection->error)die("Failed to write in DB ".var_dump($connection->error));
  }

  function readData_getLastEntry($interface,$host,$size=''){
      $connection = getDBConnection();
	  $stmt_andSize='';
	  if($size!='')$stmt_andSize.=" AND `size`='".$size."'";
      $stmt = "SELECT * FROM `data_speedtest` WHERE `interface`='".mysqli_real_escape_string($connection,$interface)."' AND  `host`='".mysqli_real_escape_string($connection,$host)."' $stmt_andSize ORDER BY timestamp DESC LIMIT 1";
	  $result = $connection->query($stmt);  
	  if($result === false )return null;
	  while($row = ($result->fetch_assoc())){    return $row;  }
	  return null;
  }

  function readData($offset=0){
	  global $measure_eachMin, $draw_maxValue__sync;
      getDBConnection();

	  //first just count rows
	  $stmt = "SELECT COUNT(*) FROM `data_speedtest`";
      $connection = getDBConnection();
      $result = $connection->query($stmt);  
	  while($row = ($result->fetch_assoc())){
		  $count_entry_rows = $row[ "COUNT(*)"];
	  }
	  
	  echo "<h2>Duration of download Testfiles </h2><p style=\"font-size:8pt;color:#555555\">Measure Time. How long its take to download a test-file. A example Value: with internet acess of ~2.400Kbit/s (300kB/s) downloading 0.1MB via DSL could take 0,3 seconds.</p>";
	  
	  //change time-period
	  $limit = 1000;
      if($offset>0 && $offset>=$limit)$next_offset = $offset+$limit; else $next_offset = 1000; 
	  echo "<p align=\"right\">";
	  echo "change Time-Period: <a href=\"?offset=".(int)$next_offset."\">< backward</a>";
	  if($offset>0 && $offset>=$limit)$next_offset = $offset-$limit; else $next_offset = 0; 
	  if($offset>0)echo " <a href=\"?offset=".(int)$next_offset."\">forward ></a>";
	  echo "</p>";
	  
	  $offset_q = ''; if($offset>0) $offset_q = ' OFFSET '.(int)$offset;
      $stmt = "SELECT * FROM `data_speedtest` ORDER BY timestamp DESC LIMIT $limit ".$offset_q;
	  //$stmt .= "(`id`, `size`, `duration`, `interface`,`timestamp`)";
      $connection = getDBConnection();
      $result = $connection->query($stmt);  
	
	  while($row = ($result->fetch_assoc())){
		  if($row['size']>0 && $row['duration']>0)
			  if(!isset($_GET['show_raw_values']))$row['duration'] = round($row['size']/$row['duration']*(1/$row['size'])*1000);
		  
		  $wertRaw[ $row['size'] ][ $row['host']  ][ $row['interface']  ][ $row['timestamp'] ] = $row['duration'];
		  $wertRaw_list_all[] = $row;
		  $wertRaw_list_eachHost[ $row['interface'] ][ $row['host'] ][] = $row;
	  }	
	  
	  
	  //find traffic_raw diffs
	  $maxTrafficRx = '';
	  foreach($wertRaw_list_eachHost as $interface => $wertRaw_list_eachHostInterface ){
	    foreach($wertRaw_list_eachHostInterface as $host => $wertRaw_list ){
		   $lastRowWithTraffic = null;
		   //$wertRaw_list = array_reverse($wertRaw_list,TRUE);var_dump($wertRaw_list);die();
		   foreach(array_reverse($wertRaw_list,TRUE) as $id => $row){
			   if($lastRowWithTraffic!=null && $row['trafficRX_raw']>$lastRowWithTraffic['trafficRX_raw']){
				   $time_diff = $row['timestamp']-$lastRowWithTraffic['timestamp'];
				   $trafficRX_raw_diff = ($row['trafficRX_raw']-$lastRowWithTraffic['trafficRX_raw']);
				   if(!isset($_GET['show_raw_values'])) $trafficRX_raw_diff = round((($trafficRX_raw_diff/$time_diff)*8)/1000); //a)von Bit in Btyes; b)von Kbit->inMbit (lasse die letzten drei Zahlen weg)..
				   if($draw_maxValue__sync){
					   if($trafficRX_raw_diff>$maxTrafficRx)$maxTrafficRx = $trafficRX_raw_diff;
				   }else $maxTrafficRx='';
				   
				   $s='';
				   for( $i=0; $i<= strlen($trafficRX_raw_diff);$i++ ){
					 $s=substr($trafficRX_raw_diff,strlen($trafficRX_raw_diff)-$i,1).$s;
					 if($i%3===0)$s=' '.$s;
				   }
				   //$s.='KB/s';
				   if($lastRowWithTraffic==null||$row['trafficRX_raw']==0)$s='';
				   if($lastRowWithTraffic['trafficRX_raw']>$row['trafficRX_raw'])$s='';
	               $wertRaw_list_eachHost[ $interface ] [ $host ] [ $id ] [ 'trafficRX_diff' ] = $s;
				   
				   
				   $trafficTX_raw_diff = $row['trafficTX_raw']-$lastRowWithTraffic['trafficTX_raw'];
				   if(!isset($_GET['show_raw_values']))$trafficTX_raw_diff = round((($trafficTX_raw_diff/$time_diff)*8)/1000); //a)von Bit in Btyes; b)von Kbit->inMbit (lasse die letzten drei Zahlen weg)..
				   $s='';
				   for( $i=0; $i<= strlen($trafficTX_raw_diff);$i++ ){
					 $s=substr($trafficTX_raw_diff,strlen($trafficTX_raw_diff)-$i,1).$s;
					 if($i%3===0)$s=' '.$s;
				   }
				   if($lastRowWithTraffic==null||$row['trafficTX_raw']==0)$s='';
				   if($lastRowWithTraffic['trafficTX_raw']>$row['trafficTX_raw'])$s='';
	               $wertRaw_list_eachHost[ $interface ] [ $host ] [ $id ] [ 'trafficTX_diff' ] = $s;
			   }
		       if($row['trafficRX_raw']!=0)$lastRowWithTraffic = $row;
			   
		   }
	    }
	  }
	  
	  //get max Values, for alle Graphs (for $draw_maxValue__sync)
	  $max = array();
	  foreach($wertRaw as $wert_size_k=>$wert_size){
		  $wert_interface_k_last='';
		  foreach($wert_size as  $wert_host_k=>$wert_host){
              if($wert_host_k==='')continue;
			  foreach($wert_host as  $wert_interface_k=>$wert_interface){ 
				 foreach($wert_interface as $time=>$value){
	                 if($draw_maxValue__sync){
		                 if(!isset($max[$wert_size_k]) || $value>$max[$wert_size_k])$max[$wert_size_k] = $value;
	                 }else $max[$wert_size_k]='';
				 }
			  }
		 }
	  }
	  
	  //var_
	  //var_dump($wertRaw);
	  $fixed_nav = '';
	  $pingGraph = '';
	  $trafficRxDiffGraph = '';
	  foreach($wertRaw as $wert_size_k=>$wert_size){
		  $wert_interface_k_last='';
		  foreach($wert_size as  $wert_host_k=>$wert_host){
              if($wert_host_k==='')continue;
			  foreach($wert_host as  $wert_interface_k=>$wert_interface){
				  if($wert_interface_k==='' || $wert_size_k==0)continue;
				  //$wert_interface[ 'timestamp' ] = $row['duration'];
				  if($wert_interface_k_last!=$wert_interface_k && count($wert_interface)>1){
				    $time_end = key($wert_interface);
				    end($wert_interface);
				    $time_start = key($wert_interface);
					  if(isset($_GET['show_raw_values']))$add='&show_raw_values=1'; else $add='';
					  if(isset($_GET['show_raw_values']))$t2='(lower=means bedder)';else $t2='(higher=means bedder)';
					  $title = 'Device: '.$wert_host_k.' '.$wert_interface_k.' '.$wert_size_k.'Mb '.$t2.':<small style="opacity:0.5"> &nbsp;&nbsp;&nbsp; <a href="#'.$wert_host_k.'_data_'.$wert_interface_k.'" title="go to data-table">go to data-table</a> &nbsp;&nbsp;&nbsp;</small><br>';
					  echo "&#11147;download: <a name=\"".$wert_host_k."_speed_".$wert_interface_k."_".$wert_size_k."\" />
					  		".$title."<img src=\"".$_SERVER['PHP_SELF']."?statsimage=1&interface=".$wert_interface_k."&size=".$wert_size_k."&host=".$wert_host_k."&start=".(int)$time_start."&end=".(int)$time_end."&max=".$max[$wert_size_k]."$add\" title=\"interface:".$wert_interface_k." size=".$wert_size_k."MB  host=".$wert_host_k."\"/><br>";
					  $pingGraph .= "&#1146;ping: <a name=\"".$wert_host_k."_ping_".$wert_interface_k."_".$wert_size_k."\" />
					  		".$title."<img src=\"".$_SERVER['PHP_SELF']."?statsimage=1&ping=1&interface=".$wert_interface_k."&size=".$wert_size_k."&host=".$wert_host_k."&start=".(int)$time_start."&end=".(int)$time_end."\" title=\"interface:".$wert_interface_k." ping time  host=".$wert_host_k."\"/><br><br>";
					  
					  $trafficRxDiffGraph .= "&#11117;traffixRxDiff: <a name=\"".$wert_host_k."_ping_".$wert_interface_k."_".$wert_size_k."\" />
					  		".$title."<img src=\"".$_SERVER['PHP_SELF']."?statsimage=1&traffic_rx_diff=1&interface=".$wert_interface_k."&size=".$wert_size_k."&host=".$wert_host_k."&start=".(int)$time_start."&end=".(int)$time_end."&max=".$maxTrafficRx."$add\" title=\"interface:".$wert_interface_k." TrafficRXDiff  host=".$wert_host_k."\"/><br><br>";
					  
					  $fixed_nav .= '
					  <p style="padding-top:5pt;" ><a href="#'.$wert_host_k.'_speed_'.$wert_interface_k.'_'.$wert_size_k.'" title="go to data-table '.$wert_host_k.'_data_'.$wert_interface_k.'"">'.$wert_host_k.' speed '.$wert_size_k.' '.$wert_interface_k.'</a>
					  <br><a href="#'.$wert_host_k.'_ping_'.$wert_interface_k.'_'.$wert_size_k.'" title="go to data-table '.$wert_host_k.'_data_'.$wert_interface_k.'"">'.$wert_host_k.' ping </a>
					  <br><a href="#'.$wert_host_k.'_data_'.$wert_interface_k.'" title="go to data-table '.$wert_host_k.'_data_'.$wert_interface_k.'">'.$wert_host_k.' data</a></p>';
                      
				  }
				  $wert_interface_k_last=$wert_interface_k;
			  }
			  $wert_interface_k_last = null;
		  }
	  }
	  echo "<div style=\"position:fixed;top:20pt;right:2pt;padding:4pt;background:rgba(255,255,255,0.85)\">$fixed_nav</div>";
	  echo "<br><hr><br>".$pingGraph;
	  echo "<br><hr><br>".$trafficRxDiffGraph;
	  
	  
	  
	  
	  echo "<table>";
	  
	  foreach($wertRaw_list_eachHost as $interface => $wertRaw_list_eachHostInterface ){
	   foreach($wertRaw_list_eachHostInterface as $host => $wertRaw_list ){
		  $lastRow = null;
		  $lastRowWithTraffic = null;
	      $check_wasOffline = 0;
		  if(strpos($_SERVER['REQUEST_URI'],'?')===FALSE)$add='?'; else $add='';
		  if(isset($_GET['show_raw_values']))$t2='(lower=means bedder)';else $t2='(higher=means bedder)';			  
		  $link_raw = "<a href=\"".$_SERVER['REQUEST_URI'].$add."&show_raw_values=1\" title=\"change to raw values each mesaure point\" style=\"font-size:7pt\">change to raw</a>";
		  $link_bandwidth =  "<a href=\"".str_replace('show_raw_values=1','',$_SERVER['REQUEST_URI'])."\" title=\"change to used bandwidth\" style=\"font-size:7pt\">change</a>";
		  $title_diff_add = "<acronym style=\"cursor:help\" title=\"~ each Second / Kbytes (diff bytes, to last measure point / time Duration)\">used</acronym><span style=\"font-size:7pt\"><br>KBit/Sec.<br>$link_raw</span>";
		  if(isset($_GET['show_raw_values']))$title_diff_add = "<acronym style=\"cursor:help\" title=\"difference between two mesaure points  (diff bytes, to last measure point)\">diff</acronym><span style=\"font-size:7pt\"><br>$link_bandwidth</span>";
		  $title_duration_or_bandwidth = 'duration<br>'.$link_bandwidth;
		  if(!isset($_GET['show_raw_values'])) $title_duration_or_bandwidth = 'bandwidth <span style="font-size:7pt"><br>KBit/Sec.<br></span>'.$link_raw;
		  echo "<tr><td colspan=\"13\"><a name=\"".$host."_data_".$interface."\"></a><br><br>Host: <b>".$host." $interface :</b></td></tr>";
		  echo "		 <tr style=\"vertical-align:bottom\">
		     <td>Time</td>
		     <td><acronym style=\"cursor:help\" title=\"Size of the downloaded test-file.\">size</acronym></td>
		     <td>$title_duration_or_bandwidth</td>
		     <td>interf.</td>
		     <td>Device</td>
		     <td><acronym style=\"cursor:help\" title=\"Mac-Adresse of connteced Wifi-AccessPoint/Hotspot (shorten)\">Hotspot</acronym></td>
		     <td><acronym style=\"cursor:help\" title=\"Signal strength in db (lower, means bedder)\">db (signal)</acronym></td>
		     <td>ping</td>
			 <td></td>
		     <td>RX Data</td>
			 <td>RX $title_diff_add</td>
		     <td>TX Data</td>
		     <td>TX $title_diff_add</td>
		     <td><small>RX Packet<br>total <span style=\"font-size:6pt\"></span></small></td>
		     <td><acronym style=\"cursor:help\" title=\"(only one char) e.g.: l=login\">action</acronym></td>
			 </tr>";
		  foreach($wertRaw_list as $id => $row){
		   $d=$row['duration'];
		   if($d==0)$d='';
		   if(isset($_GET['show_raw_values'])) $d.= ' Sec.'; 
		   echo "
		   <tr>
		     <td>".date('d.m.y H:i',$row['timestamp'])."</td>
		     <td>".$row['size']." MB</td>
		     <td style=\"text-align:right\">".$d."</td>
		     <td>".$row['interface']."</td>
		     <td>".$row['host']."</td>
		     <td>".substr($row['addr'],11,6)."</td>
		     <td>".$row['signal']."</td>
			 <td>".$row['ping']."</td>
		     ";
		   if($lastRow!=null)
			   $check_wasOffline = check_wasOffline($measure_eachMin, $lastRow['timestamp'], $row['timestamp'], $host);
		   if($check_wasOffline>0){
				    echo "<td style=\"background:white\">";
				    $arc1 = "<acronym title='Gap in the measure:  (more then ".get_measure_eachMin_indiv($row['timestamp'], $host)." Minutens) (=>possible interruption of Internet access)'>";
				    $arc2 = "</acronym>";
			        if($check_wasOffline==4) $style="color:#550000"; else $style='';
			        $offDur = ($lastRow['timestamp']-$row['timestamp'])/60;
			        if($offDur>999)$offDur="<acronym title=\"$offDur Minuten\">long time<acronym>"; else if($offDur>5)$offDur=" ".round($offDur)." Min."; else $offDur='';
				    echo " <small style=\"$style\">$arc1 gap$arc2 $offDur</small>";
			        echo " <acronym style=\"color:#888888\" title=\"symbolic: it should give a bit overview, about how many Measure-Points are missing. (possible values: |  &nbsp; ||  &nbsp; |||)\">";
			        for($i=1;$i<$check_wasOffline;$i++){echo "|";}//↯
			        echo " </acronym>";
			        echo "</td>";
		    }else echo "<td></td>";
			
	       echo "
		     <td>".$row['trafficRX']." ".$row['trafficRX_multiplicator']."</td>";
		   
		   if( isset($wertRaw_list_eachHost[ $interface ] [ $host ] [ $id ] [ 'trafficRX_diff' ] ))
			$s=$wertRaw_list_eachHost[ $interface ] [ $host ] [ $id ] [ 'trafficRX_diff' ];
		   else $s='';
		   echo "
		     <td align=\"right\">".$s."</td>";
			  
		   echo"
		     <td>".$row['trafficTX']." ".$row['trafficTX_multiplicator']."</td>";
		   
		   if( isset($wertRaw_list_eachHost[ $interface ] [ $host ] [ $id ] [ 'trafficTX_diff' ] ))
			$s=$wertRaw_list_eachHost[ $interface ] [ $host ] [ $id ] [ 'trafficTX_diff' ];
		   else $s='';
		   echo "
		     <td align=\"right\">".$s."</td>";
			  
		   echo "
		     <td>".$row['packetsRX_total']."</td>";
		   echo "
			 <td>".$row['action']."</td>
		   </tr>";
		   $lastRow = $row;
		   if($row['trafficTX_raw']!=0)$lastRowWithTraffic = $row;
		  }
	   }
	  }
	  echo "</table>";
	  echo "<br/>Count total Entry in Datebase: ".$count_entry_rows."<br/>";
  }
  function get_measure_eachMin_indiv($timestamp, $host){
	  global $measure_eachMin;
	  global $measure_eachMin_device;
	  global $measure_eachMin_second;
	  global $measure_eachMin_second_timerange;
	  $each = $measure_eachMin;
	  if(isset($measure_eachMin_device[$host])) $each = $measure_eachMin_device[$host];
	  if( $measure_eachMin_second> 0 &&
           in_array( date('H',$timestamp) , $measure_eachMin_second_timerange) ) $each = $measure_eachMin_second;
	  //$date = new Datetime($timestamp);
	  //if( in_array($date->format('H'),$measure_eachMin_second_timerange)) $each = $measure_eachMin_second;
	  return $each;
  }

  function check_wasOffline($each, $last_timestamp, $timestamp, $host){
	   $each = get_measure_eachMin_indiv($timestamp, $host);
	   if($each>0 && $last_timestamp!=null && $last_timestamp-(60*($each+1)) > $timestamp){
                if($last_timestamp-(60*(($each*12)+1)) > $timestamp)return 4;
                if($last_timestamp-(60*(($each*5)+1)) > $timestamp)return 3;
			    if($last_timestamp-(60*(($each*2)+1)) > $timestamp)return 2;
		   	  return 1;
        }
	    return 0;
  }

  function readDataAndDrawStat(){
	  global $measure_eachMin; //just for diagramm title
	  global $measure_eachMin_device; //just for diagramm title

	  $connection = getDBConnection();
	  $offset_q = ''; if( isset($_GET['offset']) && $_GET['offset']>0) $offset_q = ' OFFSET '.(int)$_GET['offset'];
	  $where_add = ''; if( isset($_GET['start']) && $_GET['start']>0 && $_GET['end']>0) $where_add = ' AND `timestamp`>='.mysqli_real_escape_string($connection,$_GET['start']).' AND `timestamp`<='.mysqli_real_escape_string($connection,$_GET['end']).' ';
	  

      $stmt = "SELECT * FROM `data_speedtest` WHERE `interface`='". mysqli_real_escape_string($connection,$_GET['interface'])."' AND `host`='". mysqli_real_escape_string($connection,$_GET['host'])."' AND `size`='". mysqli_real_escape_string($connection,$_GET['size'])."' ".$where_add." ORDER BY timestamp DESC LIMIT 1000 ".$offset_q;
      
      $result = $connection->query($stmt); 
	  //die($stmt);
	  if($result===false)die('no values');

	  $last_addr = '';$last_addr_ts='';
	  $werte = array();$accessPoints=array();$werte_ping=array();$werteTrafficRxDiff=array();
	  while($row = ($result->fetch_assoc())){
		  $t=$row['timestamp'];
		  
		  if(isset($_GET['show_raw_values'])) $werte[ $t ] = $row['duration'];
		  else $werte[ $t ] =  round($row['size']/$row['duration']*(1/$row['size'])*1000);
		  
		  if( $row['ping']!='')$werte_ping[ $t ] = $row['ping'];
		  if( $last_addr!='' && $row['addr']!='' && $row['addr'] != $last_addr){
			  $accessPoints[ $t ] = substr($last_addr,11,6); 
		  }
		  $last_addr = $row['addr'];
		  $last_addr_ts = $t;
		  
	  	  if(isset($last_trafficRx_row) && $row['trafficRX_raw']!='' && 
			 $last_trafficRx_row['trafficRX_raw']>$row['trafficRX_raw']){
		    $diff = $last_trafficRx_row['trafficRX_raw'] - $row['trafficRX_raw'];
			$t_diff = $last_trafficRx_row['timestamp'] - $row['timestamp'];
			$werteTrafficRxDiff[ $t ] =   (($diff/$t_diff)*8/1000);
			if(isset($_GET['show_raw_values'])){
				$werteTrafficRxDiff[ $t ] = $diff;
				/*compensate measure gaps (??untested/bug?)
				$s=get_measure_eachMin_indiv( $t, $row['host']);
				if($s!=$measure_eachMin){
					  if( $s>$measure_eachMin )$werteTrafficRxDiff[ $t ] = $diff/($s);
					  if( $s<$measure_eachMin )$werteTrafficRxDiff[ $t ] = $diff/($s);
				}*/
			}
		  }
		  $last_trafficRx_row = $row;
	  }
	  if($last_addr_ts!='')$accessPoints[ $last_addr_ts ] = substr($last_addr,11,6);
      
	  if( !isset($_GET['ping']) && !isset($_GET['traffic_rx_diff']) ){
		 $t= array("test bandwidth via download of ".$_GET['size']."Mb","KBit/sec.");
		 if(isset($_GET['show_raw_values'])) $t= array("duration to download ".$_GET['size']."Mb", "sec.");
		 draw($t,$werte,$_GET['size'],$_GET['host'],$accessPoints,isset($_GET['max'])?$_GET['max']:'');
      }else if( isset($_GET['traffic_rx_diff'])){
		 $each = $measure_eachMin;
		 if(isset($measure_eachMin_device[$_GET['host']])) $each = $measure_eachMin_device[$_GET['host']];
		 $t= array("TrafficRX used bandwidth","KBytes/Sec.");
		 if(isset($_GET['show_raw_values'])) $t= array("TrafficRX count between the MeasurePoint (~possible every ? $each Minutes)","?Bytes");
		  draw( $t,$werteTrafficRxDiff,$_GET['size'],$_GET['host'],$accessPoints,isset($_GET['max'])?$_GET['max']:'',-1);
	  }else if(  isset($_GET['ping']))
		 draw('test',$werte_ping,$_GET['size'],$_GET['host'],$accessPoints,'');


  }


  function draw($title, $wert,$size, $host, $accessPoints, $max,$draw_maxValue_act=null){
	  global $measure_eachMin, $draw_maxValue, $backgroundLinesPersonal, $draw_maxValue__sync, $config_hide_in_draw__empty_items;
	  if(!isset($draw_maxValue_act) || $draw_maxValue_act!=-1) $draw_maxValue_act = $draw_maxValue;
    if(count($wert)==0) die("no values");
	  
	  ksort($wert);
	  /*
	  $i=0;
	  $w2 = array();
      foreach($wert as $k=>$v){
	  	$i++;
		  if($i<=500)$w2[ $k ] = $v;
	  }
	  $wert = $w2;*/
	  $width = '1000';
	  $height = '200';
	  $imgWidth = $width+50;
	  $imgHeight = $height+50;

	  $theImage  = imagecreate($imgWidth+100, $imgHeight+70);
	  $colorGrey = imagecolorallocate($theImage, 225, 225, 225);
	  $colorGreyDark = imagecolorallocate($theImage, 100, 100, 100);
	  $colorBlue = imagecolorallocate($theImage, 0, 50, 255);
	  $colorOrange1 = imagecolorallocate($theImage, 255, 157, 5);
	  $colorOrange2 = imagecolorallocate($theImage, 255, 119, 5);
	  $colorRed = imagecolorallocate($theImage, 255, 5, 5);
	  $black = imagecolorallocate($theImage, 0, 0, 0);
	  $gray  = imagecolorallocate($theImage, 102, 102, 102);
	  $white = imagecolorallocate($theImage, 255, 255, 255);


	  $maxHeight = 0;
	  $maxValue = 0;
	  #based on from https://webmaster-glossar.de/zeichnen-mit-php-ein-liniendiagramm.html
	  $countBalken = count($wert);
	  foreach($wert as $w){
		  if($maxHeight < $w){
			  $maxHeight = $w;
			  $maxValue = $w;
		  }
	  }
	  if($max!=''){ $maxHeight = $max; $maxValue = $max;}
	  if($maxHeight==1){
		  foreach($wert as &$w){
			  $w = $w*25;
			  if($maxHeight < $w){
				  $maxHeight = $w;
			  }
		  }
	  }	

	  
	  if( $draw_maxValue_act!=-1 && isset($draw_maxValue[ $size ]) && $maxValue>$draw_maxValue[ $size ] && $draw_maxValue[ $size ]!=''){
		  if(isset($_GET['show_raw_values'])){
		      $maxValue=$draw_maxValue[ $size ];
		      $maxHeight=$draw_maxValue[ $size ];
		  }
	  }
	  if( isset($_GET['ping'])){ $maxValue = 100; $maxHeight=100;  }
	
      $arry  = array($maxValue,$maxValue/4*3,$maxValue/2,$maxValue/4,0);
	  $arry2 = $arry;
	  
	  if(isset($_GET['ping'])){
		  if(isset($backgroundLinesPersonal[ $_GET['host'] ]['ping']))foreach($backgroundLinesPersonal[ $_GET['host'] ]['ping'] as $p)$arry2[]=$p;
	  }else if(isset($_GET['traffic_rx_diff'])){
	  
	  }else{
		  if(isset($_GET['show_raw_values'])){
			if(isset($backgroundLinesPersonal['raw'][ $_GET['host'] ]['normal']))
				foreach($backgroundLinesPersonal['raw'][ $_GET['host'] ]['normal'] as $p)$arry2[]=$p;
		  }else{
			if(isset($backgroundLinesPersonal[ $_GET['host'] ]['normal']))
				foreach($backgroundLinesPersonal[ $_GET['host'] ]['normal'] as $p)$arry2[]=$p;
		  }
	  }
	  
	  $x_ori = 0;
      $y_ori = 0;
	  $y_bottom = 0;
	  $xlen = $width - $x_ori - $x_ori * 1.5;
      $ylen = $height - $y_ori - $y_bottom;
	  
	  $dynWidth = ($imgWidth-2) / ($countBalken-1);
	  @$dynHeight = ($imgHeight-1) / $maxHeight;
      
	  #draw background lines
	  $i=0;
      foreach($arry2 as $a){
		  $i++;
		  $c = count($arry2)-1;
          $y = $imgHeight-$dynHeight*$a;
          if($i>count($arry))$style=array($white,$white,$white,$white,$white);
		  else $style=array($white,$white,$gray,$white,$white);
          imagesetstyle($theImage,$style);
          imageline($theImage,$x_ori+2,$y,$x_ori+70+40 + $xlen -2,$y,IMG_COLOR_STYLED);
		  $n=number_format($a,1);
          $y2 = $y-7; //if($i==0)$y2+=$c;
		  //die('a'. ($x_ori+70+40) );
		  //$maxValue
		  if(strlen($maxValue)>5) $move= -(strlen($maxValue)*5); else $move=0;
		  imagestring($theImage,2,$x_ori+70+40 + $xlen + 5 + $move,$y2,$n, $black);               
      }
	  
	if( isset($_GET['ping'])){  $text1 = "ping";                              $text2 = "ms";    }
	else{                       $text1 = "duration to download ".$size."Mb";  $text2 = "sec.";  }
	if($title!="test" && count($title)==2){ $text1 = $title[0];  $text2 = $title[1]; }
    imagestring($theImage,4,10,50,$text1, $colorGreyDark);
	if(strlen($text2)>4)$v=strlen($text2)*5;else $v=0;
    imagestring($theImage,4,$x_ori+70+40 + $xlen + 5-$v,$imgHeight+25,$text2, $black);
	
    reset($wert);
    $timestampStart  = key($wert);
    end($wert);
    $timestampEnd    = key($wert);
    $timestampLength = $timestampEnd - $timestampStart;
    reset($wert);
	  
    $eachSeconds_inPixel = $xlen/$timestampLength;
    //die($timestampLength);
    
    
    #delete empty ones
	if($config_hide_in_draw__empty_items){
	  foreach($wert as $timestamp=>$w){
	  	if($w==0.0)unset($wert[$timestamp]);
	  }
    }
    
    #draw dates
    $timestamp = $timestampStart;
    $last_t=0;
    $last_day='';
    $last_h='';
    while($timestamp<=$timestampEnd){
      $tObje = new DateTime(  );
      $tObje->setTimestamp($timestamp);
      $tObje->setTime( $tObje->format('H'), 0, 0);
      $timestamp=$tObje->getTimestamp();
      
      $x = (int)@round($eachSeconds_inPixel * ($timestamp-$timestampStart));
      if($last_day!=date('d.M.Y',$timestamp) && $x>20){
        //if($countBalken<301){
            imagestring($theImage,2,$x,$imgHeight+45,"|".date('d.M.',$timestamp), $black);
        /*}elseif( $countBalken<601){
            if($last_m!= date('M',$timestamp)) imagestring($theImage,1,$ix,$imgHeight+25,"|".date('M',$timestamp), $black);
            imagestring($theImage,1,$x,$imgHeight+45,"|".date('d',$timestamp), $black);
        }elseif( date('d',$timestamp)%2==0){
            if($last_m!= date('M',$timestamp)){
              imagestring($theImage,1,$x,$imgHeight+25,"|".date('M',$timestamp), $black);
            }
            imagestring($theImage,1,$x,$imgHeight+45,"|".date('d',$timestamp), $black);
            }*/
      }
      
      //draw "hours"
		  $lineHour = 1;
		  if( count($wert)>1001 ) $lineHour = 12;
		  if($last_h!=date('H',$timestamp) && date('H',$timestamp)%$lineHour==0 ){
			  imagestring($theImage,1,$x,$imgHeight+25,"|".date('H',$timestamp), $black);
			  $last_h = date('H',$timestamp);
		  }
      
      $last_day = date('d.M.Y',$timestamp);
      $last_m = date('M',$timestamp);
      $last_h = date('H',$timestamp);
      $tObje->modify('+1 Hour');
      $timestamp = $tObje->getTimestamp();
    }
    
      
	  #draw diagram
	  $i = 0;
	  $last_day=0;
	  $last_timestamp = null;
	  //for testing: $wert = array(key($wert)+0=>1,key($wert)+5000=>1,key($wert)+10000=>2,key($wert)+15000=>2,key($wert)+20000=>3,key($wert)+25000=>3,key($wert)+30000=>4,key($wert)+35000=>4,key($wert)+40000=>0,key($wert)+45000=>0);
	  foreach($wert as $timestamp=>$w){
		  if(next($wert)===FALSE)continue; //end
	    $value_next = current($wert);
	    $timestamp_next = key($wert);
		  $y = @round($dynHeight * $w);
		  $y_next = @round($dynHeight * $value_next);
		  $x = @round($eachSeconds_inPixel * ($timestamp-$timestampStart) );
		  $x_next = @round($eachSeconds_inPixel * ($timestamp_next-$timestampStart));
		  
		  
		  if($i+1 < $countBalken){
			  $col = $colorBlue;		#
			  $check_wasOffline = check_wasOffline($measure_eachMin, $timestamp_next, $timestamp, $host);
			  if( $check_wasOffline == 1 )      $col = $colorOrange1;
			  else if( $check_wasOffline == 2 ) $col = $colorOrange2;
			  else if( $check_wasOffline > 2 )  $col = $colorRed;
			  //imageline($theImage, $i*$dynWidth, ($imgHeight-$aktNewSize),
			  //($i+1)*$dynWidth, ($imgHeight-$aktNewSize2), $col);
			  //if( round(($timestamp_next-$timestamp-(60*10))/100)*100/60>$default_measureGap_inMin)$col=$colorOrange1;
			  imageline($theImage, $x, ($imgHeight-$y), $x_next, ($imgHeight-$y_next), $col);
		  }
		  

		  if( isset( $accessPoints[$timestamp] ) ){
		  	  imagestring($theImage,2,$x,0+5,"|".$accessPoints[$timestamp], $black);
		  }
		  $i++;
		  $last_timestamp = $timestamp;	
	  }
	  
      imagestring($theImage,1,10,$imgHeight+60,"imprecise: dues to measure gaps: compression in X-Axis possible; any past/future diagramm's are different in X-axis and not compare-able.", $gray); //this means:  There are shown X (e.g. 100) last entrys. And this entry's are streched/compressed in one single image (anway ist a week, or just a hour)
	  
      header("Content-type: image/png");
	  imagepng($theImage);
	  imagedestroy($theImage);
  }

  function getDBConnection(){
	  global $dbname, $dbusername, $dbpassword, $dburl;
      //Erzeugt einen Datenbanklink aus den Parametern (die Verbindungsdaten stehen in der Datei dbconfig.php)
      $link = mysqli_connect($dburl, $dbusername, $dbpassword,$dbname);
      if (!$link) {
        die('connection to Database failed: ' . mysqli_error());
      }
      /* check connection */
      if (mysqli_connect_errno()) {
        die($fehler1);
      }
      return $link;
  }



  /*


  CREATE TABLE `data_speedtest` (
    `id` int(11) NOT NULL,
    `size` decimal(6,1) NOT NULL COMMENT 'in MB',
    `duration` decimal(6,2) NOT NULL,
    `interface` varchar(15) NOT NULL,
    `timestamp` bigint(20) NOT NULL,
    `addr` varchar(20) NOT NULL,    
    `signal` int(3) NOT NULL,    
    `trafficRX` decimal(5,1) NOT NULL,    
    `trafficRX_multiplicator` varchar(1) NOT NULL,  
	`trafficRX_raw` INT(15) NOT NULL
    `trafficTX` decimal(5,1) NOT NULL,    
    `trafficTX_multiplicator` varchar(1) NOT NULL,   
	`trafficTX_raw` INT(15) NOT NULL,
    `packetsRX_total` int(11) NOT NULL,       
    `ping` int(5) NOT NULL,
    `action` VARCHAR(7) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  --
  -- Indizes der exportierten Tabellen
  --

  --
  -- Indizes für die Tabelle `data`
  --
  ALTER TABLE `data`
    ADD PRIMARY KEY (`id`);

  --
  -- AUTO_INCREMENT für exportierte Tabellen
  --

  --
  -- AUTO_INCREMENT für Tabelle `data`
  --
  ALTER TABLE `data`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
    */

  ?>
