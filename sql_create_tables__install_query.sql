
  
  CREATE TABLE `data_speedtest` (
    `id` int(11) NOT NULL,
    `size` decimal(6,1) NOT NULL COMMENT 'in MB',
    `duration` decimal(6,2) NOT NULL,
    `interface` varchar(15) NOT NULL,
    `timestamp` bigint(20) NOT NULL,
    `addr` varchar(20) NOT NULL,    
    `signal` int(3) NOT NULL,    
    `trafficRX` decimal(5,1) NOT NULL,    
    `trafficRX_multiplicator` varchar(1) NOT NULL,  
	`trafficRX_raw` INT(15) NOT NULL
    `trafficTX` decimal(5,1) NOT NULL,    
    `trafficTX_multiplicator` varchar(1) NOT NULL,   
	`trafficTX_raw` INT(15) NOT NULL,
    `packetsRX_total` int(11) NOT NULL,       
    `ping` int(5) NOT NULL,
    `action` VARCHAR(7) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  --
  -- Indizes der exportierten Tabellen
  --
  --
  -- Indizes für die Tabelle `data`
  --
  ALTER TABLE `data`
    ADD PRIMARY KEY (`id`);
  --
  -- AUTO_INCREMENT für exportierte Tabellen
  --
  --
  -- AUTO_INCREMENT für Tabelle `data`
  --
  ALTER TABLE `data`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
   
